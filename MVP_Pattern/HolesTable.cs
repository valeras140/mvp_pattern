﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_Pattern
{
    public class HolesTable
    {
        private Dictionary<string, float[]> data = new Dictionary<string, float[]>();
        private static int ID_counter = 1;
        private int id;
        private string name;

        public int Id
        {
            get { return id; }
        }
       
        public string Name
        {
            get { return name; }
            set { name = value; }
        } 
        
        public HolesTable(string name)
        {
            Name = name;
            id = ++ID_counter;           
        }

        public void AddRow(string holeName, float xParam, float yParam)
        {
            float[] pair = new float[2];
            pair[0] = xParam;
            pair[1] = yParam;
            data.Add(holeName, pair);
        }

        public void GetHoleInfoByName(string holeName, out float x, out float y)
        {
            float[] pair;
            data.TryGetValue(holeName, out pair);
            x = pair[0];
            y = pair[1];
        }

        public string[] GetHolesName()
        {
            return data.Keys.ToArray();
        }

    }
}
