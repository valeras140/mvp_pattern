﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_Pattern
{
    class Presenter
    {
        private IModel _model = new Model();
        private IView _view;

        public Presenter(IView view)
        {
            _view = view;
            _view.SetFormula += new EventHandler<EventArgs>(OnSetFormula);
            _view.SetParameterX += new EventHandler<EventArgs>(OnSetParameterX);
            _view.SetParameterY += new EventHandler<EventArgs>(OnSetParameterY);
            _view.UpdateChart += new EventHandler<EventArgs>(OnUpdateChart);
            //RefreshView();
        }
        
        private void OnSetFormula(object sender, EventArgs e)
        {
            _model.SetFormula("");
        }

        private void OnSetParameterX(object sender, EventArgs e)
        {
            _model.SetParameterX("");
        }

        private void OnSetParameterY(object sender, EventArgs e)
        {
            _model.SetParameterY("");
        }
        
        // Обновление Представления новыми значениями модели.
        private void OnUpdateChart(object sender, EventArgs e)
        {
            _view.UpdatePlot(_model.GetTable("nameTable"));
        }

        // По сути Binding (привязка) значений модели к Представлению. 
        

        
    }
}
