﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_Pattern
{
    interface IModel
    {
        HolesTable GetTable(string tableName);
        void SetFormula(string formula);
        void SetParameterX(string xParam);
        void SetParameterY(string yParam);
    }

    class Model : IModel
    {
        HolesTable dt;
        string formula = "f1";
        string xParam = "p1";
        string yParam = "p2";

        public void SetFormula(string formula)
        {
            this.formula = formula;
        }

        public void SetParameterX(string xParam)
        {
            this.xParam = xParam;
        }

        public void SetParameterY(string yParam)
        {
            this.yParam = yParam;
        }

        //формирует таблицу с данными 
        public HolesTable GetTable(string tableName)
        {
            dt = new HolesTable(tableName);

            /*
            //реализация цикла по скважинам и глибинам по формуле и занесение результатов в таблицу
            ...
            */

            dt.AddRow("hole One", 0.2f, 0.5f);
            dt.AddRow("hole Two", 0.9f, 0.7f);
            
            return dt;
        }






    }
}
