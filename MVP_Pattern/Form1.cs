﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.WindowsForms;
using OxyPlot.Series;

namespace MVP_Pattern
{
    public partial class Form1 : Form
    {
        public Form1()
        {           
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            var myModel = new PlotModel { Title = "Example 1" };
            myModel.Series.Add(new FunctionSeries(Math.Cos, 0, 10, 0.1, "cos(x)"));
            dotDiagram.Model = myModel;
        
          
            /* //тест Модели
            Model m = new Model();
            DataTable dataTb = m.GetTable("SOIL", "PORO", "max", "table1");

            string[] holes = dataTb.GetHoleNames();

            float x, y;
            dataTb.GetXY(holes[1], out x, out y);

            label1.Text = holes[0].ToString();
            label2.Text = x.ToString();
            label3.Text = y.ToString();
            */

        }

        private void panel1_Move(object sender, EventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char s = e.KeyChar;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox3.Text = "";
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
