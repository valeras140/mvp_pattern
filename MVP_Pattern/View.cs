﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_Pattern
{

    public interface IView
    {
        void UpdatePlot(HolesTable dt);


        event EventHandler<EventArgs> SetFormula;
        event EventHandler<EventArgs> SetParameterX;
        event EventHandler<EventArgs> SetParameterY;
        event EventHandler<EventArgs> UpdateChart;
        

    }
    /*
    public partial class View : Form1, IView
    {
        public void UpdatePlot(HolesTable dt)
        {
            MessageBox.Show("UpdatePlot. Hole 0 name: " + dt.GetHolesName()[0]);
            //отрисовка графика и подписи на осях
        }

    }*/
}